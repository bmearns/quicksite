#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

from django.test import TestCase, Client

class UserStory4(TestCase):
    """
    Tests to ensure directory index pages work.
    """

    def test_index_1(self):
        client = Client()
        r1 = client.get('/qs/pg/')
        self.assertEqual(r1.status_code, 200)
        r2 = client.get('/qs/pg/index.md')
        self.assertEqual(r2.status_code, 200)
        self.assertEqual(r1.content, r2.content)


class UserStory3(TestCase):
    """
    Tests to ensure you can't access files outside of the document root.
    """

    def setUp(self):
        pass

    def test_access_index_1(self):
        client = Client()
        response = client.get('/qs/pg/index.md')
        self.assertEqual(response.status_code, 200)

    def test_access_index_2(self):
        client = Client()
        response = client.get('/qs/pg/')
        self.assertEqual(response.status_code, 200)

    def test_access_index_3(self):
        client = Client()
        response = client.get('/qs/pg/d1')
        self.assertEqual(response.status_code, 200)

    def test_access_index_4(self):
        client = Client()
        response = client.get('/qs/pg/d1/')
        self.assertEqual(response.status_code, 200)

    def test_access_index_5(self):
        client = Client()
        response = client.get('/qs/pg/d1/index.md')
        self.assertEqual(response.status_code, 200)

    def test_noaccess_1(self):
        client = Client()
        response = client.get('/qs/pg/../test.md')
        self.assertEqual(response.status_code, 403)
        
    def test_noaccess_2(self):
        client = Client()
        response = client.get('/qs/pg/../test.md/')
        self.assertEqual(response.status_code, 403)
        
    def test_noaccess_3(self):
        client = Client()
        response = client.get('/qs/pg/../test.md')
        self.assertEqual(response.status_code, 403)
        
    def test_noaccess_4(self):
        client = Client()
        response = client.get('/qs/pg/../test_does_not_exist.md')
        self.assertEqual(response.status_code, 403)
        

