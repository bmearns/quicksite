#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

import django
from django.core.management import call_command
from django.conf import settings
from django.conf.urls import patterns, include, url
import sys

from types import ModuleType


default_settings = dict(
    TEMPLATE_DEBUG = True,
    TEMPLATE_DIRS = ['templates'],

    INSTALLED_APPS = (
        'django.contrib.contenttypes',
        'django.contrib.staticfiles',

        #Third party apps
        'django_mathjax',

        #Custom apps
        'quicksite',
    ),

    MIDDLEWARE_CLASSES = (
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ),

    DEBUG = True,

    STATIC_URL = '/static/',

    #django_mathjax
    MATHJAX_ENABLED=True,
    MATHJAX_CONFIG_FILE = "TeX-AMS-MML_HTMLorMML",
    MATHJAX_CONFIG_DATA = {
        "tex2jax": {
          "inlineMath":
            [
                ['$','$'],
            ]
        }
    },

    ROOT_URLCONF = 'url_conf',

)



def main():

    import argparse

    parser = argparse.ArgumentParser(
        description=(
            'Launch a django quicksite to serve files from the file system.'
        ),
    )
    parser.add_argument(
        '-p', '--port',
        dest='port',
        metavar='PORT',
        action='store',
        type=int,
        default=8000,
        help=(
            'Specify the port number to server on. Default is 8000.'
        ),
    )
    parser.add_argument(
        '-r', '--root', '--content-root',
        dest='content_root',
        metavar='PATH',
        action='store',
        default='.',
        help = (
            'Specify the path to the content root, the base directory from which files '
            'will be served. The default is the current directory. ALL FILES IN THE SPECIFIED '
            'DIRECTORY MAY BE ACCESSIBLE FROM THE SITE.'
        ),
    )

    options = parser.parse_args()

    settings.configure(**default_settings)

    from quicksite import urls
    from django.conf.urls.static import static

    url_conf = ModuleType('url_conf')
    url_conf.urlpatterns = patterns('',
        url(r'^/?', include(urls, namespace='qs', app_name='quicksite'), dict(content_root=options.content_root)),
    ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    sys.modules['url_conf'] = url_conf

    django.setup()
    call_command('runserver', '0.0.0.0:%d' % (options.port,))


if __name__ == '__main__':
    main()

