#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

from django.shortcuts import render
from django.template import Context
from django.views.generic import View
from django.views.defaults import server_error
from django.http import HttpResponse, Http404, HttpResponseForbidden
from django.core.urlresolvers import resolve

from django.utils.safestring import mark_safe
from django.utils import html as Html

import mimetypes
import markdown

import os.path

class FileSystemView(View):

    content_root = '__site_contents'
    site_title = None

    def get(self, request, path, content_root = None):

        self.request = request

        content_root = self.content_root = content_root or self.content_root
        abs_content_root = os.path.realpath(self.content_root)
        if not abs_content_root.endswith(os.sep):
            abs_content_root += os.sep

        #Strip off trailing slashes.
        while path and path[-1] == '/':
            path = path[:-1]
        self.path = path
            
        #Split it into a trail of path components.
        trail = self.trail = path.split('/')
        if path:
            self.default_title = trail[-1]
        else:
            self.default_title = 'Home'

        #Get the file-system path to the source file.
        fs_base = os.path.realpath(os.path.join(self.content_root, *trail))

        #Make sure it's contained by the content root.
        fs_base_plus = fs_base
        if not fs_base_plus.endswith(os.sep):
            fs_base_plus += os.sep
        if not fs_base_plus.startswith(abs_content_root):
            return HttpResponseForbidden()

        #Make sure the path exists. It's either a file or a directory.
        is404 = False
        if not os.path.exists(fs_base):
            is404 = True

        if os.path.isfile(fs_base):
            fs_path = fs_base
            #If they are explicitly asking for an index file, the default title is for the directory.
            if trail[-1] == 'index.md':
                if len(trail) > 1:
                    self.default_title = trail[-2]
                else:
                    self.default_title = 'Home'
        else:
            #Try to serve an index file.
            fs_path = os.path.join(fs_base, 'index.md')
            if not os.path.exists(fs_path):
                is404 = True

        ext = os.path.splitext(fs_path)[-1].lower()
        if is404:
            raise Http404
        elif ext == '.md':
            return self.serve_markdown(fs_path)
        elif ext in ('.jpg', '.jpeg', '.png', '.gif', '.bmp', '.xpm', '.xbm'):
            return self.serve_image(fs_path, ext)
        else:
            raise Http404

    def serve_image(self, fs_path, fs_ext):
        ct, enc = mimetypes.guess_type(fs_path)
        if ct is None:
            return server_error(self.request)

        with open(fs_path, 'rb') as istream:
            content = istream.read()
        resp = HttpResponse(content, content_type=ct)
        if enc is not None:
            resp['Content-Encoding'] = enc

        return resp

    def serve_markdown(self, fs_path):

        with open(fs_path, 'rb') as istream:
            source = istream.read()

        #Make sure we have a unicode object, things get tricky otherwise.
        #TODO: Could look for specially formatted lines in the text that specify the encoding in ASCII.
        if not isinstance(source, unicode):
            source = unicode(source, 'latin-1')

        #get the current app
        current_app = resolve(self.request.path).namespace

        md = markdown.Markdown(
            extensions = [
                'quicksite.mdext.escape',

                'extra',
                'admonition',
                'codehilite',
                'headerid(level=3)',
                'sane_lists',
                'smarty(smart_quotes=False)',
                'toc(permalink=True)',
                'footnotes',
                'meta',

                'quicksite.mdext.wikilinks(current_app=%s)'  % current_app,
                'quicksite.mdext.ext.dellsystem.wikinotes.mdx.mdx_mathjax',
                'quicksite.mdext.ext.dellsystem.wikinotes.mdx.mdx_urlize',

            ],
            safe_mode='escape',
        )

        rendered = mark_safe(md.convert(source))

        ctx = dict(
            rendered_content = rendered,
            title = md.Meta.get('title', [self.default_title])[0],
            document_meta = md.Meta,
        )
        resp = render(self.request, 'quicksite/page.html', ctx, current_app=current_app)

        return resp

