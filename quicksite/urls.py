#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

from django.conf.urls import patterns, url
from django.views.generic import RedirectView

import views

urlpatterns = patterns('',

    url(r'^/?$', RedirectView.as_view(url='pg/')),

    url(r'^pg/(?P<path>.*)$', views.FileSystemView.as_view(), name='page'),

)

