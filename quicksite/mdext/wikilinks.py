#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

import markdown
from django.core.urlresolvers import reverse

class WikiLinksPattern(markdown.inlinepatterns.Pattern):

    def __init__(self, current_app):
        self.current_app = current_app
        markdown.inlinepatterns.Pattern.__init__(
            self,
            r'(\!\!?)?\[\['
            r'('
                r'(?:'
                    r'[^\]]'
                    r'|'
                    r'\][^\]]'
                r')+'
            r')'
            r'\]\]'
            r'([-\w]*)'
        )

    def getUrl(self, page):
        target_app = self.current_app
        _parts = page.rsplit(':', 1)
        if len(_parts) > 1:
            target_app, page = _parts

        url = reverse('quicksite:page', kwargs=dict(path=page), current_app=target_app)
        return url, page


    def getNode(self, page, title=None, tail=None, bang=False, double_bang=False):
        
        url, page = self.getUrl(page)
        title = title or page
        tail = tail or ''

        if bang:
            node = markdown.util.etree.Element('img', src=url, alt=title + tail)
            node.set('class', '_internal')
            if not double_bang:
                img = node
                node = markdown.util.etree.Element('a', href=url, title=page)
                node.set('class', '_internal')
                node.append(img)
        else:
            node = markdown.util.etree.Element('a', href=url, title=page)
            node.set('class', '_internal')
            node.text = title + tail
        return node
        

    def handleMatch(self, m):
        bang_text = m.group(2)
        bang = bool(bang_text)
        double_bang = bang and len(bang_text) == 2
        match = m.group(3).strip()
        parts = match.split('|', 1)

        tail = m.group(4)

        title = None
        if len(parts) == 1:
            #No pipe.
            page = match
        else:
            page = parts[0].strip()
            title = parts[1].strip()

        return self.getNode(page, title, tail, bang, double_bang)


class WikiLinksExtension(markdown.Extension):

    config = {
        'current_app': ['', 'The instance namespace for the current_app.'],
    }

    def extendMarkdown(self, md, md_globals):
        md.inlinePatterns.add('wikilinks', WikiLinksPattern(self.getConfig('current_app')), '_end')

def makeExtension(**configs):
    return WikiLinksExtension(configs)



