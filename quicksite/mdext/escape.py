#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

import markdown
import markdown.util
from django.core.urlresolvers import reverse

class EscapePattern(markdown.inlinepatterns.Pattern):
    """
    Outputs an empty string, but is useful for breaking up things that would otherwise
    be joined. In particular, if you want to have non-linked text connected to the end of a wiki-link.
    """

    def __init__(self):
        markdown.inlinepatterns.Pattern.__init__(
            self ,
            r'<<'
            r'((?:'
                r'[^>]'
                    r'|'
                r'>[^>]'
            r')*)'
            r'>>'
        )

    def handleMatch(self, m):
        contents = m.group(2)
        if not contents:
            return ''
        node = markdown.util.etree.Element('span')
        node.set('class', '_escaped')
        node.text = markdown.util.AtomicString(contents)
        return node

class EscapeDocPattern(markdown.inlinepatterns.Pattern):
    """
    Outputs an empty string, but is useful for breaking up things that would otherwise
    be joined. In particular, if you want to have non-linked text connected to the end of a wiki-link.
    """

    def __init__(self):
        markdown.inlinepatterns.Pattern.__init__(
            self ,
            r'<<'
            r'\!([A-Z]+)\s+'
            r'(.*?)'
            r'\2>>'
        )

    def handleMatch(self, m):
        contents = m.group(3)
        if not contents:
            return ''
        node = markdown.util.etree.Element('span')
        node.set('class', '_escaped _escapedoc')
        node.text = markdown.util.AtomicString(contents)
        return node

class EscapeExtension(markdown.Extension):
    def extendMarkdown(self, md, md_globals):
        md.inlinePatterns.add('escape', EscapePattern(), '_begin')
        md.inlinePatterns.add('escapedoc', EscapeDocPattern(), '<escape')

def makeExtension(**configs):
    return EscapeExtension(configs)



