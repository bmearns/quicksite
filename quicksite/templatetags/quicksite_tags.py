#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

from django import template
from django.core.urlresolvers import reverse

import markdown.util

from quicksite.mdext.wikilinks import WikiLinksPattern

register = template.Library()

@register.simple_tag(takes_context=True)
def qs_url(context, page):
    wikilinks = WikiLinksPattern(context.current_app)
    url, page = wikilinks.getUrl(page)
    return url

@register.simple_tag(takes_context=True)
def qs_link(context, page, title=None):
    wikilinks = WikiLinksPattern(context.current_app)
    node = wikilinks.getNode(page, title)
    return markdown.util.etree.tostring(node)
    

@register.simple_tag(takes_context=True)
def qs_img(context, page, title=None):
    wikilinks = WikiLinksPattern(context.current_app)
    node = wikilinks.getNode(page, title, bang=True)
    return markdown.util.etree.tostring(node)
    
    

