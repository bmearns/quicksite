#! /usr/bin/env python
# vim: set fileencoding=utf-8: set encoding=utf-8:

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView

import quicksite.urls

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    url(r'^/?$', RedirectView.as_view(url='qs/')),

    url(r'^qs/', include(quicksite.urls, namespace='qs1', app_name='quicksite')),
    url(r'^qs2/', include(quicksite.urls, namespace='qs2', app_name='quicksite'), dict(content_root='__site2')),
)
