
Design Decisions
=====================

File extensions in URL
-----------------------------

Two possibilities: include the file extension in the url, like :path:`/site/pg/my/article.md`, or do not include it,
like :path:`/site/pg/my/article`. I'm opting for the latter, not including it, and we will automatically look for a
file with an appropriate file extension based on a prioritized list of file extensions.

There are some downsides: you can't have a file with the same name but a different extension and have them both served
as pages. In other words, which ever file has the highest priority file extension will hide other files with the same
base name. This is pretty easy to work around by giving it a double extension, like :path:`article.html.html`.

The upsides are that the URLs are nicer, they don't reveal the syntax you're using, and you can therefore change the syntax
for a page without changing the URL.

On the otherhand, things are just a lot easier if I do include the file extension, and I think it still gives the freedom
to go back and remove them later without breaking compatibility. Also, I can provide some tools to automatically apply a file
extension to a URL based on directory or pattern matching or something.

