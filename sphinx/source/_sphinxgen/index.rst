
==============================================================
quicksite Documentation
==============================================================

**quicksite** is a python package.

Documentation Contents:
------------------------------

.. toctree::
   :maxdepth: 2

   README


   LICENSE


Indices and tables
--------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Version
-------------------

|version-badge|

This documentation is for quicksite |release|.


Project Resources
------------------------
* `quicksite project homepage (bitbucket) <https://bitbucket.org/bmearns/>`_
* `quicksite on pypi <https://pypi.python.org/pypi/quicksite>`_
* Online documentation:
    * `Read The Docs (.org) <http://quicksite.readthedocs.org/>`_
    * `Python Hosted (.org) <http://pythonhosted.org/quicksite/>`_
