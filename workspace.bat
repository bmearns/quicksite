
@ECHO OFF

start console -w "SERVER QUICKSITE" -d "%~dp0\demo"
start console -w "UNITTEST QUICKSITE" -d "%~dp0\demo"
start console -w "GIT QUICKSITE" -d "%~dp0"
start console -w "SPHINX QUICKSITE" -d "%~dp0"

start cmd /c cd "%~dp0" ^&^& start gvim -p -c ":simalt ~x"

if "%PYTHON_HOME%"=="" GOTO NO_HH
    start hh "%PYTHON_HOME%.\Doc\python275.chm"
:NO_HH
